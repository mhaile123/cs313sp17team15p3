package edu.luc.etl.cs313.android.shapes.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.R.attr.x;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		Shape s = f.getShape();
		return s.accept(this);
	}

	@Override
	public Location onGroup(final Group g) {

		List<? extends Shape> shapes = g.getShapes();
		for(Shape s: shapes){
			s.accept(this);
		}

	}

	@Override
	public Location onLocation(final Location l) {

		Shape s = l.getShape();
		return s.accept(this);
	}

	@Override
	public Location onRectangle(final Rectangle r) {

		int x = r.getWidth();
		int y = r.getHeight();
		return new Location(-x, -y, r);
	}

	@Override
	public Location onStroke(final Stroke c) {
		Shape s = c.getShape();
		return s.accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {

		Shape s = o.getShape();
		return s.accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> pts = s.getPoints();
		List<Integer> x = new ArrayList<>();
		List<Integer> y = new ArrayList<>();
		for(Point p : pts){
			x.add(p.getX());
			y.add(p.getY());
		}
		int x1 = x.size();
		int y1 = y.size();
		Collections.sort(x);
		Collections.sort(y);
		int x0 = x.get(0);
		int y0 = y.get(0);
		return new Location(x.get(0), y.get(0), new Rectangle(x.get(x1) - x0, y.get(y1) - y0));
	}
}
