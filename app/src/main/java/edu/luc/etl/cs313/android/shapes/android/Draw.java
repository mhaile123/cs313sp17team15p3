package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import java.util.List;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint = paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		int x = c.getRadius();
		canvas.drawCircle(-x, -x, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		Shape s = c.getShape();
		s.accept(this);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Shape s = f.getShape();
		s.accept(this);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		List<? extends Shape> gro = g.getShapes();
		for (Shape s : gro){
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		Shape s = l.getShape();
		s.accept(this);
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {

		canvas.drawRec(r, paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {

		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		final float[] pts = null;

		canvas.drawLines(pts, paint);
		return null;
	}
}
